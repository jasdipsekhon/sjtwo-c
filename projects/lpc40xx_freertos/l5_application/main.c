#include <stdio.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "gpio_lab.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"

// 'static' to make these functions 'private' to this file
static void create_blinky_tasks(void);
static void create_uart_task(void);
static void blink_task(void *params);
static void uart_task(void *params);

// static void task_one(void *task_parameter);
// static void task_two(void *task_parameter);
void led_task(void *task_parameter);
void switch_task(void *task_parameter);
void led_task_part_1(void *pvParameters);
void led_task_part_2(void *pvParameters);

static SemaphoreHandle_t switch_press_indication;

typedef struct {
  /* First get gpio0 driver to work only, and if you finish it
   * you can do the extra credit to also make it work for other Ports
   */
  // uint8_t port;

  uint8_t pin;
} port_pin_s;

/*******************************************************************************
 *
 *                      M A I N    F U N C T I O N
 *
 ******************************************************************************/

int main(void) {
  create_blinky_tasks();
  create_uart_task();

  // If you have the ESP32 wifi module soldered on the board, you can try uncommenting this code
  // See esp32/README.md for more details
  // uart3_init();                                                                     // Also include:  uart3_init.h
  // xTaskCreate(esp32_tcp_hello_world_task, "uart3", 1000, NULL, PRIORITY_LOW, NULL); // Include esp32_task.h

  // xTaskCreate(led_task, "led", 2048 / sizeof(void *), NULL, PRIORITY_LOW, NULL);
  // xTaskCreate(task_one, "Task one", 4096 / sizeof(void *), NULL, 1, NULL);
  // xTaskCreate(task_two, "Task two", 4096 / sizeof(void *), NULL, 1, NULL);

  // TODO:
  // Create two tasks using led_task() function
  // Pass each task its own parameter:
  // This is static such that these variables will be allocated in RAM and not go out of scope

  switch_press_indication = xSemaphoreCreateBinary();
  // static port_pin_s led0 = {0};
  // static port_pin_s led1 = {1};
  static port_pin_s led = {24};
  static port_pin_s sw = {15};
  xTaskCreate(led_task, "led2", 2048 / sizeof(void *), &led, PRIORITY_HIGH, NULL);
  xTaskCreate(switch_task, "switch", 2048 / sizeof(void *), &sw, PRIORITY_HIGH, NULL);
  // xTaskCreate(led_task, "led1", 2048 / sizeof(void *), NULL, PRIORITY_LOW, &led1);

  puts("Starting RTOS");
  vTaskStartScheduler(); // This function never returns unless RTOS scheduler runs out of memory and fails

  return 0;
}

void led_task(void *task_parameter) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(task_parameter);
  gpio0__set_as_output(led->pin);
  while (true) {
    if (xSemaphoreTake(switch_press_indication, 1000)) {
      gpio0__set_low(led->pin); // LED is OFF
    } else {
      gpio0__set_high(led->pin); // LED is ON
      puts("Timeout: No switch press indication");
    }
  }
}

void switch_task(void *task_parameter) {
  const port_pin_s *sw = (port_pin_s *)(task_parameter); // typecast parameter and save to variable
  gpio0__set_as_input(sw->pin);                          // set switch pin direction to input
  while (true) {
    if (gpio0__get_level(sw->pin)) { // if pin is high/pressed give semaphore to led_task
      xSemaphoreGive(switch_press_indication);
    }
    vTaskDelay(100);
  }
}
void led_task_part_1(void *pvParameters) {
  // Choose one of the onboard LEDS by looking into schematics and write code for the below
  // 0) Set the IOCON MUX function(if required) select pins to 000
  LPC_IOCON->P1_24 = 0b000;

  // 1) Set the DIR register bit for the LED port pin
  LPC_GPIO1->DIR |= (1 << 24); // Set direction: Output = 1
  while (true) {
    // 2) Set PIN register bit to 0 to turn ON LED (led may be active low)
    LPC_GPIO1->PIN |= (1 << 24); // SET LED2 (P1.24) to ON state
    vTaskDelay(500);

    // 3) Set PIN register bit to 1 to turn OFF LED
    LPC_GPIO1->PIN &= ~(1 << 24); // SET LED2 (P1.24) to OFF state
    vTaskDelay(500);
  }
}
void led_task_part_2(void *pvParameters) {
  // Type-cast the paramter that was passed from xTaskCreate()
  const port_pin_s *led = (port_pin_s *)(pvParameters);
  gpio0__set_as_output(led->pin);

  while (true) {
    // 2) Set PIN register bit to 0 to turn ON LED (led may be active low)
    // LPC_GPIO1->PIN |= (1 << 24); // SET LED2 (P1.24) to ON state
    gpio0__set_high(led->pin);
    // *gpioPort1OutputSet = 1;
    vTaskDelay(500);

    // 3) Set PIN register bit to 1 to turn OFF LED
    // LPC_GPIO1->PIN &= ~(1 << 24); // SET LED2 (P1.24) to OFF state
    gpio0__set_low(led->pin);
    // *gpioPort1OutputClr = 1;
    vTaskDelay(500);
  }
  // Choose one of the onboard LEDS by looking into schematics and write code for the below
  // 0) Set the IOCON MUX function(if required) select pins to 000
  // LPC_IOCON->P1_24 = 0b000;
  // gpio0__set_as_output(24);

  // 1) Set the DIR register bit for the LED port pin
  // uint32_t *pin_function_memory = (uint32_t *)0x4002C0E0; // Port 1 Pin 24 memory address (Table )

  // LPC_GPIO1->DIR |= (1 << 24); // Set direction: Output = 1

  // Port 1 registers (Table 94)

  // uint32_t *gpioPort1DirectionControl = (uint32_t *)0x20098020; // Port 1 direction control register
  // uint32_t *gpioPort1PinValue = (uint32_t *)0x20098034;         // Port 1 pin value register
  // uint32_t *gpioPort1OutputSet = (uint32_t *)0x20098038;        // Port 1 output set register
  // uint32_t *gpioPort1OutputClr = (uint32_t *)0x2009803C;        // Port 1 output clear register

  // *gpioPort1DirectionControl = 1; // Set direction: Output = 1
  // *gpioPort1PinValue = 24;        // Select port 24
}

/*
static void task_one(void *task_parameter) {
  while (true) {
    // Read existing main.c regarding when we should use fprintf(stderr...) in place of printf()
    // For this lab, we will use fprintf(stderr, ...)
    fprintf(stderr, "AAAAAAAAAAAA");

    // Sleep for 100ms
    vTaskDelay(100);
  }
}
static void task_two(void *task_parameter) {
  while (true) {
    fprintf(stderr, "bbbbbbbbbbbb");
    vTaskDelay(100);
  }
}
*/
static void create_blinky_tasks(void) {
  /**
   * Use '#if (1)' if you wish to observe how two tasks can blink LEDs
   * Use '#if (0)' if you wish to use the 'periodic_scheduler.h' that will spawn 4 periodic tasks, one for each LED
   */
#if (1)
  // These variables should not go out of scope because the 'blink_task' will reference this memory
  static gpio_s led0, led1;

  // If you wish to avoid malloc, use xTaskCreateStatic() in place of xTaskCreate()
  static StackType_t led0_task_stack[512 / sizeof(StackType_t)];
  static StackType_t led1_task_stack[512 / sizeof(StackType_t)];
  static StaticTask_t led0_task_struct;
  static StaticTask_t led1_task_struct;

  led0 = board_io__get_led0();
  led1 = board_io__get_led1();

  xTaskCreateStatic(blink_task, "led0", ARRAY_SIZE(led0_task_stack), (void *)&led0, PRIORITY_LOW, led0_task_stack,
                    &led0_task_struct);
  xTaskCreateStatic(blink_task, "led1", ARRAY_SIZE(led1_task_stack), (void *)&led1, PRIORITY_LOW, led1_task_stack,
                    &led1_task_struct);
#else
  periodic_scheduler__initialize();
  UNUSED(blink_task);
#endif
}

static void create_uart_task(void) {
  // It is advised to either run the uart_task, or the SJ2 command-line (CLI), but not both
  // Change '#if (0)' to '#if (1)' and vice versa to try it out
#if (0)
  // printf() takes more stack space, size this tasks' stack higher
  xTaskCreate(uart_task, "uart", (512U * 8) / sizeof(void *), NULL, PRIORITY_LOW, NULL);
#else
  sj2_cli__init();
  UNUSED(uart_task); // uart_task is un-used in if we are doing cli init()
#endif
}

static void blink_task(void *params) {
  const gpio_s led = *((gpio_s *)params); // Parameter was input while calling xTaskCreate()

  // Warning: This task starts with very minimal stack, so do not use printf() API here to avoid stack overflow
  while (true) {
    gpio__toggle(led);
    vTaskDelay(500);
  }
}

// This sends periodic messages over printf() which uses system_calls.c to send them to UART0
static void uart_task(void *params) {
  TickType_t previous_tick = 0;
  TickType_t ticks = 0;

  while (true) {
    // This loop will repeat at precise task delay, even if the logic below takes variable amount of ticks
    vTaskDelayUntil(&previous_tick, 2000);

    /* Calls to fprintf(stderr, ...) uses polled UART driver, so this entire output will be fully
     * sent out before this function returns. See system_calls.c for actual implementation.
     *
     * Use this style print for:
     *  - Interrupts because you cannot use printf() inside an ISR
     *    This is because regular printf() leads down to xQueueSend() that might block
     *    but you cannot block inside an ISR hence the system might crash
     *  - During debugging in case system crashes before all output of printf() is sent
     */
    ticks = xTaskGetTickCount();
    fprintf(stderr, "%u: This is a polled version of printf used for debugging ... finished in", (unsigned)ticks);
    fprintf(stderr, " %lu ticks\n", (xTaskGetTickCount() - ticks));

    /* This deposits data to an outgoing queue and doesn't block the CPU
     * Data will be sent later, but this function would return earlier
     */
    ticks = xTaskGetTickCount();
    printf("This is a more efficient printf ... finished in");
    printf(" %lu ticks\n\n", (xTaskGetTickCount() - ticks));
  }
}
